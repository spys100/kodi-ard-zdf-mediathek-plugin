SOURCES = main.py addon.xml Readme.md
LIB_SRC = library/mbase.py library/kikamediathek.py library/ardmediathek.py library/zdfmediathek.py library/mexception.py library/__init__.py
LIB_DIR = library
RESOURCE_DIR = resources
VERSION = 1.1.2
TARGET = ardzdfmediathek-$(VERSION).zip
TMP_DIR = plugin.video.ardzdfmediathek

all: $(TARGET)

$(TMP_DIR): $(SOURCES) $(RESOURCE_DIR) $(LIB_SRC)
	@mkdir -p $@/$(LIB_DIR)
	@cp $(LIB_SRC) $@/$(LIB_DIR)
	@cp -r $(SOURCES) $(RESOURCE_DIR) $@/

$(TARGET): $(SOURCES) $(RESOURCE_DIR) $(LIB_SRC) $(TMP_DIR)
	zip $@ -r $(TMP_DIR) 

clean: 
	rm -f $(TARGET)
	rm -rf $(TMP_DIR)

.PHONY: clean all 
