from __future__ import print_function
from mock import Mock, patch
#Mock up any calls to modules that cannot be imported
xbmc = Mock()
xbmcgui = Mock()
xbmcaddon = Mock()

modules = {
    'xbmc' : xbmc,
    'xbmcgui': xbmcgui,
    'xbmcaddon': xbmcaddon
    }

module_patcher = patch.dict('sys.modules', modules) #@UndefinedVariable
module_patcher.start()

import xbmcaddon
import xbmc

xbmc.log = print
xbmc.getUserAgent.return_value = 'test'
addon=xbmcaddon.Addon.return_value
addon.getAddonInfo.return_value = "/tmp/"
import sys
from library.zdfmediathek import ZDFMediathek
from library.ardmediathek import ARDMediathek
from library.kikamediathek import KIKAMediathek

print("--- home test KIKA ---")
m=KIKAMediathek('home')
el=m.get_elements()
cat=m.get_categories()
m.fetch_mediathek_layout(el[0]['mid'])

print("--- atoz test ARD ---")
m=ARDMediathek('atoz')
el=m.get_elements()
cat=m.get_categories()
m.fetch_mediathek_layout(el[0]['mid'])
el=m.get_elements("A") 

print("--- atoz test ZDF ---")
m=ZDFMediathek('atoz')
el=m.get_elements()
cat=m.get_categories()
m.fetch_mediathek_layout(el[0]['mid'])
el=m.get_elements("A")

for Mediathek in ZDFMediathek,ARDMediathek:
    m=Mediathek('time')
    el=m.get_elements()
    m.fetch_mediathek_layout(el[0]['mid'])
    m=Mediathek('home')
    cat=m.get_categories()
    el=m.get_elements()
    mid = None
    for e in el:
        if e['type'] == 'video': 
            mid=e['mid']
            break
    m.get_video_desc(mid)
