# -*- coding: utf-8 -*-

'''
encapsulates ARD Mediathek API
'''
from mbase import MediathekBase, catEPG
from xbmc import log as dbg
from xbmc import LOGWARNING, LOGERROR, LOGINFO
from datetime import date, time

#import  xbmcplugin

_ROOT_KEY_ = '$_this_$'
    
class ARDMediathek(MediathekBase):

    urlTemplate = 'http://tv.ardmediathek.de/dyn/get?id={0}&client=ard'
    name = 'ard'
    def get_video_desc(self, mid):
        '''
        :return dict {'url':'http://...','synopsis':'Lorem ipsum ...'}
        '''
        if str(mid).startswith('video'):
            desc = self._fetch_json( mid, 'hbbtv')
            if desc['id'] == 'error':
                return {'error':desc['error'],'msg': desc['elems'][0]['text']}
            else:
                video = desc['video']
                media = video['streams'][0]['media'][0]
                return  {'url': media['url'],
                    'type': media['mimeType'],
                    'synopsis': video['meta']['synopsis'] if 'synopsis' in video['meta'] else '',
                    'duration': video['meta']['durationSeconds'] if 'durationSeconds' in video['meta'] else 0 }
        else:
            dbg('unknown mid type ' + mid)
            return ''
        

        '''
        :param jsn: structure 
            elems: [{
                type: "epg", "day": 20200503,
                epgDays: [
                    { "name": "DI 28.04.", "dayid": 20200428 },
                    ... ]
                channels: [
                    { id: "daserste", 
                      "epgA": [
                        {
                            "tim": 330,
                            "dur": 50,
                            "tit": "Das Waisenhaus für wilde Tiere",
                            "link": {
                                "type": "video",
                                "id": mid
                            },
                            "sub": "1 Video"
                        },... ]
                      epgB: ...,
                      epgC: ...,
                      epgD: ..., 
                    }
                ]
            }] 
         :rtype: see fetch_mediathek_layout
        '''
        def _today_id():
            t = date.today()
            return t.strftime('%Y%m%d')
        def _min2time(offset):
            h = (int(offset) / 60) % 24
            m = int(offset) % 60
            t = time(h,m)       
            return t # .strftime('%H:%M')
        def _dayid2cat(dayid):
            for el in jsn['elems']:
                if el['type'] == 'epgdays':
                    epgday = el['epgDays']
                    if epgday['dayid'] == dayid:
                        return epgday['name']
            return ''
        rv = {}
        #default is yesterday
        daycat = _dayid2cat(jsn['elems'][0]['day'])
        if dayid == None:
            rv = { _ROOT_KEY_ : [] }
            days = rv[_ROOT_KEY_]
            for day in jsn['elems'][0]['epgDays']:
                entry = {
                    'img': '', 'imgHi': '',
                    'title': day['name'], 'subtitle': '',
                    'type': 'subcategory',
                    'mid': 'time:{0}'.format(day['dayid'])
                    }
                days.append(entry)
        else:
            channels = []
            for channel in jsn['elems'][0]['channels']:
                centry = {
                    'img': self._resource_path + channel['id'] + '.png',
                    'imgHi': '',
                    'title': channel['id'], 'subtitle': '',
                    'type': 'subcategory',
                    'mid': 'time:{0}'.format(dayid)
                    }
                bcasts = []
                for section in ('epgA','epgB','epgC','epgD'):
                    for video in channel[section]:
                        if 'link' not in video: continue
                        timestr = _min2time(video['tim']).strftime('%H:%M')
                        entry = {
                            'img': '',
                            'imgHi': '',
                            'title': timestr + ' ' +  video['tit'], 
                            'subtitle': video['sub'] if 'sub' in video else '',
                            'type': 'video',
                            'duration': video['dur'],
                            'mid': video['link']['id']
                            #'mid': 'time:{0}'.format(dayid)
                            }
                        bcasts.append(entry)
                channels.append(centry)
                rv.update({ channel['id'] : bcasts })
            rv.update({daycat: channels})
        return rv

    def __parse_element(self, e):
        duration = 0
        if 'dur' in e and len(e['dur']) > 0:
            duration = int(e['dur'].split()[0]) * 60
        #dbg(str(e))
        if 'title' in e and e['title'] != '':
            title = e['title']
        else:
            title = e['cpixTitle'] if 'cpixTitle' in e else '-?-'
        if 'time' in e:
            title = u'({1}) {0}'.format(title,e['time'])
        entry = {
            'img': e['img'] if 'img' in e else '',
            'imgHi': e['imgHi'] if 'imgHi' in e else '',
            'title': title,
            'subtitle': e['subtitle'] if 'subtitle' in e else '',
            'type': 'video' if e['link']['type'] == 'video' else 'subcategory',
            'mid': e['link']['id'],
            'duration': duration
            }
        if entry['img'] == '' and 'posterimg' in e :
            entry['img'] = e['posterimg']
        if entry['imgHi'] == '' and 'posterimgHi' in e :
            entry['img'] = e['posterimgHi']
        return entry

    def fetch_mediathek_layout(self, mid):
        '''
        Get the list of videofiles/streams.
    
        Here you can insert some parsing code that retrieves
        the list of video streams in the given category from some site or server.
        
        :param category: Category name
        :type category: str
        :return the list of videos in the category
        :rtype: dict with schema
                category: the category name 
                [{
                    img: low res image
                    imgHi: high res image 
                    title: the title of the video or subcategory
                    subtitle: Subtitle
                    type: 'video' or 'subcategory'
                    mid: media id
                }, ...]
        '''
    
        # base category
        category = _ROOT_KEY_
        self._current_layout = { _ROOT_KEY_ : [] }

        rv = self._fetch_json( mid )
        # handle epg 
        if False and mid.startswith('time'):
            dayid = mid.split(':',1)
            if len(dayid) > 1:
                dayid = dayid[1]
            else:
                dayid = None
            self._current_layout = self.__get_epg_layout(rv, dayid)
            return 
        # special handling of a-z categories 
        if mid == 'atoz':
            self._current_layout[category] = self._generate_atoz_elements(map(lambda i: chr(i),range(ord('a'),ord('z')+1)))
        #elif mid == 'time':
        #    category = catEPG
        # filter video elements
        for item in rv['elems']:
            if item['type'] == 'message' and not mid.startswith('time'):
                # each message defines the category for the following videos
                category = item['title']
            elif item['type'] == 'epgdays' and mid == 'time':
                days = self._current_layout[category]
                # currentDayId = item['day']
                for day in item['epgDays']:
                    dayid = day['dayid']
                    entry = {
                        'img': '', 'imgHi': '',
                        'title': day['name'], 'subtitle': '',
                        'type': 'subcategory',
                        'mid': 'time:'+ dayid
                        }
                    days.append(entry)
            elif item['type'] == 'chcovers' and mid != 'time':
                channels = self._current_layout[category]
                # currentDayId = item['day']
                for ch in item['elems']:
                    entry = {
                        'img': '', 'imgHi': '',
                        'title': ch['client'], 'subtitle': '',
                        'type': 'subcategory',
                        'mid': ch['link']['id']
                        }
                    channels.append(entry)
            elif item['type'] == 'tabbedcovers':
                for item in item['elems']:
                    if item['type'] in [ 'stage', 'covers', 'vcovers']:
                        # select video elements
                        for e in item['elems']:
                            entry = self.__parse_element(e)
                            if category in self._current_layout:
                                self._current_layout[category].append(entry)
                            else:
                                self._current_layout[category] = [entry] 
            elif item['type'] in [ 'stage', 'covers', 'vcovers']:
                # select video elements
                for e in item['elems']:
                    entry = self.__parse_element(e)
                    if category in self._current_layout:
                        self._current_layout[category].append(entry)
                    else:
                        self._current_layout[category] = [entry]
        
    
    def _generate_atoz_elements(self,subcats):
        '''
            :return: list of a-z elements
        '''
        atoz = []
        for c in subcats:
            entry = {
                'img': '',
                'imgHi': '',
                'title': c,
                'subtitle': '',
                'type': 'subcategory',
                'mid': 'atoz:' + c}
            atoz.append(entry)
        return atoz
    
  
        