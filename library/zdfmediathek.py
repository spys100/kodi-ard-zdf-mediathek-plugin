# -*- coding: utf-8 -*-

'''
encapsulates ZDF hbbtv Mediathek API
'''

from mbase import MediathekBase
from xbmc import log as dbg
from xbmc import LOGWARNING, LOGERROR, LOGINFO
from xbmc import getUserAgent
from datetime import date, time
from mexception import MException

#import  xbmcplugin
    
class ZDFMediathek(MediathekBase):  
    urlTemplate = 'http://hbbtv.zdf.de/zdfm3/dyn/get.php?id={0}'
    name = 'zdf'
    def __select_stream_url(self, streams):
        '''
        :return some stream url or None if none found
        :rtype: string,string
        '''
        pref_lang="deu"
        pref_q='q3'
        pref_m3u=True
        url = None
        mime = 'video/mp4'
        for stream in streams:
            for fmt,stype in filter(lambda i: type(i[1])==dict ,stream.items()) :
                dbg('vid fmt: {}'.format(fmt),LOGWARNING)
                if 'mpd'in fmt:
                    mime = 'application/dash+xml'
                elif 'm3u' in fmt:
                    mime = 'application/x-mpegurl'
                else:
                    mime = 'video/mp4'
                for q,u in stype['main'][pref_lang].items():
                    url = u
                    if q==pref_q: break
                if mime == 'application/x-mpegurl' and pref_m3u: break
        return url,mime

    def get_video_desc(self, mid):
        '''
        :return dict {'url':'http://...','synopsis':'Lorem ipsum ...'}
        '''
        desc = self._fetch_json( mid, 'hbbtv')
        if desc['id'] == 'error':
            return {'error':desc['error'],'msg': desc['elems'][0]['text']}
        else:
            url,mime = self.__select_stream_url(desc['streams'])
            return  {'url': url,
                'type': mime,
                'synopsis': desc['text'] if 'text' in desc else '',
                'duration': desc['duration'] if 'duration' in desc else '0' }

    def __parse_covers(self, cover, category):
       # videos = {}
       # videos[category] = []
        for e in cover['elems']:
                duration = 0
                if 'infoline' in e and e['infoline'] != None and len(e['infoline']['text']) > 0 and e['infoline']['text'].endswith('min') :
                    duration = int(e['infoline']['text'].split()[0]) * 60
                #dbg(str(e))
                entry = {
                    'img': e['img'] if 'img' in e else '',
                    'imgHi': e['imgHi'] if 'imgHi' in e else '',
                    'title': e['titletxt'],
                    'subtitle': e['foottxt'] if 'foottxt' in e else '',
                    'type': 'video' if e['link']['type'] == 'video' else 'subcategory',
                    'mid': e['link']['id'],
                    'duration': duration
                    }
                if entry['img'] == '' and 'posterimg' in e :
                    entry['img'] = e['posterimg']
                if entry['imgHi'] == '' and 'posterimgHi' in e :
                    entry['img'] = e['posterimgHi']

                if category in self._current_layout:
                    self._current_layout[category].append(entry)
                else:
                    self._current_layout[category] = [entry]

    def fetch_mediathek_layout(self, mid):
        '''
        Get the list of videofiles/streams.
        
        :param category: Category name
        :type category: str
        :return nothing
        :rtype: dict with schema
                category: the category name 
                [{
                    img: low res image
                    imgHi: high res image 
                    title: the title of the video or subcategory
                    subtitle: Subtitle
                    type: 'video' or 'subcategory'
                    mid: media id
                }, ...]
        '''
        dbg('fetch mid {0}'.format(mid),LOGWARNING)
        # base category
        category = MediathekBase._ROOT_KEY_
        fmid = mid
        if mid == 'home': fmid = 'zdf-hbbtv-startseite-100'
        if mid == 'atoz': fmid = 'special:atoz'
        if mid == 'time': fmid = 'special:time'
        rv = self._fetch_json( fmid )
        
        # filter video elements
        for item in rv['elems']:
            # special handling of a-z categories 
            if mid == 'atoz':
                if item['type'] == 'dropdown':
                    for o in item['options']:
                        entry = {
                            'img': '',
                            'imgHi': '',
                            'title': o['name'],
                            'subtitle': '',
                            'type': 'subcategory',
                            'mid': 'special:atoz:'+ str(o['id']),
                            'duration': 0
                        }
                        self._current_layout[category].append(entry)
            elif item['type'] == 'list':
                for day in item['elems']:   
                    entry = {
                        'img': day['img'], 'imgHi': day['img'],
                        'title': day['titletxt'], 'subtitle': '',
                        'type': 'subcategory',
                        'mid': day['link']['id']
                        }
                    self._current_layout[category].append(entry)
            elif item['type'] == 'covers':
                # title defines the category for the following videos
                if item['title'] == '':
                    category = MediathekBase._ROOT_KEY_
                else:
                    category = item['title']
                # select video elements
                self.__parse_covers(item,category)
    