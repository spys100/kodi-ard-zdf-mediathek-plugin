# -*- coding: utf-8 -*-

'''
encapsulates KiKa Mediathek API
'''
from mbase import MediathekBase
from xbmc import log as dbg
from xbmc import LOGWARNING, LOGERROR, LOGINFO
from datetime import date, time

#import  xbmcplugin

_ROOT_KEY_ = '$_this_$'
    
class KIKAMediathek(MediathekBase):

    urlTemplate = 'http://cdn.hbbtv.kika.de/player/dyn/get.php?id={0}'
    name = 'kika'
    def get_video_desc(self, mid):
        '''
        :return dict {'url':'http://...','synopsis':'Lorem ipsum ...'}
        '''
        desc = self._fetch_json( mid)
        if desc['ok']:
            # select highest quality
            media = desc['assets'][0]
            for m in desc['assets']:
                if m['qual'] > media['qual']:
                    media = m
            return  {'url': media['url'],
                'type': media['mtype'],
                'duration': 0,
                'synopsis': ''}
        else:
            return {}

    def __parse_element(self, e):
        def selectImg(match):
            img = ''
            sel = list(filter(lambda k: (match in str(k)),e.keys()))
            for k in sel:
                if str(k).startswith('extraLarge'):
                    img = e[k]
                    break
                if str(k).startswith('large'):
                    img = e[k]
                    break
                if str(k).startswith('medium'):
                    img = e[k]
                    break
                if str(k).startswith('small'):
                    img = e[k]
                    break
            return img
        duration = 0
        if 'duration' in e:
            duration = int(e['duration'])
        #dbg(str(e))
        entry = {
            'img': selectImg('TeaserImage'),
            'imgHi': selectImg('BackgroundImage'),
            'title': e['title'] if 'title' in e and e['title'] != '' else '-?-',
            'subtitle': str.format('Noch {0!s} Tage',e['expire']) if 'expire' in e else '',
            'type': 'video' if e['profile'] == 'video' else 'subcategory',
            'mid': ('player:' if e['profile'] == 'video' else (e['profile'] + ':')) + str(e['id']),
            'duration': duration
            }
        if 'description' in e:
            entry['synopsis'] = e['description']
        return entry

    def fetch_mediathek_layout(self, mid):
        '''
        Get the list of videofiles/streams.
    
        Here you can insert some parsing code that retrieves
        the list of video streams in the given category from some site or server.
        
        :param category: Category name
        :type category: str
        :return the list of videos in the category
        :rtype: dict with schema
                category: the category name 
                [{
                    img: low res image
                    imgHi: high res image 
                    title: the title of the video or subcategory
                    subtitle: Subtitle
                    type: 'video' or 'subcategory'
                    mid: media id
                }, ...]
        '''
    
        # base category
        category = _ROOT_KEY_
        self._current_layout = { category:[] }

        fmid = mid
        if mid == 'home': fmid = 'special:home'
 
        rv = self._fetch_json( fmid )

        if 'items' in rv:
            embedded_mids = {}
            for item in rv['items']:
                entry = self.__parse_element(item)
                self._current_layout[category].append(entry)
                if ( mid == 'home'
                    and '_embedded' in item
                    and 'brand' in item['_embedded']):
                    entry = self.__parse_element(item['_embedded']['brand'])
                    if entry['mid'] not in embedded_mids:
                        embedded_mids.update({ entry['mid']: None })
                        self._current_layout[category].append(entry)
        
    
    
  
        