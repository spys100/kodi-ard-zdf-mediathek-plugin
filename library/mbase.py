
import json
from urllib2 import Request, urlopen
from mexception import MException
from xbmc import LOGWARNING, LOGERROR, LOGINFO, getUserAgent
from xbmcaddon import Addon
from xbmc import log as dbg

_FETCH_TIMEOUT = 10000 # timeout in ms

catEPG='Sendung verpasst?'
catSubCat='_'

class MediathekBase:
    
    _ROOT_KEY_ = '$_this_$'
    _resource_path = Addon().getAddonInfo('path') + '/resources/media/'

    def __init__(self, mid=None):
        '''
        :param mid: a category identifier in path form 
               i.e <root>/sub1/sub2/...
        '''
        #ida = mid.split('/',2)
        #self._id = ida[0] 
        #self._subid = '' if len(ida) == 1 else ida[1]
        #self._category = category
        #self._context = xmbcplugin.getProperty('context') 
        self._current_layout = { MediathekBase._ROOT_KEY_ : [] }
        if mid == None: return
        dbg('res path {0}'.format(self._resource_path),LOGWARNING)
        self.fetch_mediathek_layout(mid) 

    def _fetch_json(self, mid, fmt=None):
        def check_for_error(jsn):
            if 'error' in jsn:
                message = jsn['error']
                if 'elems' in jsn:
                    message = jsn['elems'][0]['text']
                raise MException(message)
        url = self.urlTemplate.format(mid)
        if fmt != None:
            url = url + '&format=' + fmt
        req = Request(url, headers={'User-agent': getUserAgent()})
        fo = urlopen(req, timeout=_FETCH_TIMEOUT)
        jsn =  json.load(fo,'utf-8')
        check_for_error(jsn) # throws PlayError
        return jsn

    def get_elements(self, category=_ROOT_KEY_):
        '''
            returns all elements in category.
            :param category: if not set return root 
            :return:
            :rtype: list 
        '''
        try:
            #return list(filter(lambda i: (i['type'] == 'video'), self._current_layout[category]))
            return self._current_layout[category]
        except KeyError:
            dbg('category {0} not found'.format(category),LOGWARNING)
            return []

    def get_categories(self):
        '''
            :return all categories except special _ROOT_KEY_
        '''
        return list(filter(lambda i: ( i != MediathekBase._ROOT_KEY_ ), self._current_layout.keys() ))
        #return self._current_layout.keys()
        # cat = []
        # append all sub categories
        # for c in categories:
        #    cat.extend(list(filter(lambda i: (i['type'] == 'subcategory'), 
        #                           self._current_layout[c])))
        # cat.extend(categories)
        #return cat
        
