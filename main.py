# -*- coding: utf-8 -*-
# Module: default
# Author: Carsten K
# License: GPL v.3 https://www.gnu.org/copyleft/gpl.html

REMOTE_DBG = False

import sys
from library.mexception import MException
from library.ardmediathek import ARDMediathek
from library.zdfmediathek import ZDFMediathek
from library.kikamediathek import KIKAMediathek
from library.mbase import catEPG,catSubCat

from urllib import urlencode
from urlparse import parse_qsl
import xbmcgui
import xbmcplugin
from xbmc import log as dbg
from xbmc import LOGWARNING, LOGERROR, LOGINFO
        
# Get the plugin url in plugin:// notation.
_url = sys.argv[0]
# Get the plugin handle as an integer number.
_handle = int(sys.argv[1])

_mediathek = None

if REMOTE_DBG:
    # Make pydev debugger works for auto reload.
    # Note pydevd module need to be copied in XBMC\system\python\Lib\pysrc
    try:
        import pydevd # with the addon script.module.pydevd, only use `import pydevd`
    # stdoutToServer and stderrToServer redirect stdout and stderr to eclipse console
        pydevd.settrace('localhost', stdoutToServer=True, stderrToServer=True)
    except ImportError:
        sys.stderr.write("Error: " +
            "You must add org.python.pydev.debug.pysrc to your PYTHONPATH.")
        sys.exit(1)
    

def get_url(**kwargs):
    """
    Create a URL for calling the plugin recursively from the given set of keyword arguments.

    :param kwargs: "argument=value" pairs
    :type kwargs: dict
    :return: plugin call URL
    :rtype: str
    """
    def encoded_dict(in_dict):
        out_dict = {}
        for k, v in in_dict.iteritems():
            if isinstance(v, unicode):
                v = v.encode('utf8')
            elif isinstance(v, str):
                # Must be encoded in UTF-8
                v.decode('utf8')
            out_dict[k] = v
        return out_dict
    return '{0}?{1}'.format(_url, urlencode(encoded_dict(kwargs)))
    

        
def insert_element(category,mid, item): 
   
    bcaster_name = mid if _mediathek == None else _mediathek.name
    plot = ''
    duration = -1
    # is_folder = True means that this item opens a sub-list of lower level items.
    is_folder = item == None or item['type'] == 'subcategory'
    # Set graphics (thumbnail, fanart, banner, poster, landscape etc.) for the list item.
    mediatype = 'item' if is_folder else 'video'
       
    if item != None:       
        title = item['title']
        tagline = item['subtitle']
        plot = item['synopsis'] if 'synopsis' in item else tagline
        duration = item['duration'] if 'duration' in item else -1
        # Create a list item with a text label and a thumbnail image.
        list_item = xbmcgui.ListItem(label=title,label2=item['subtitle'])
        list_item.setArt({'thumb': item['img'], 
                          'icon': item['img'],
                          'fanart': item['imgHi']})
        if is_folder:
            url = get_url(action='listing',category=catSubCat, mid=item['mid'], bcaster=bcaster_name )
        else:
            list_item.setProperty('IsPlayable', 'true')
            #get desc from video id
            #desc = ARDMediathek.get_video_desc(item['mid'])
            #plot = desc['synopsis']
            url = get_url(action='play', vid=item['mid'], bcaster=bcaster_name )
    else:
        title = category
        tagline = ''
        
        # Create a list item with a text label and a thumbnail image.
        list_item = xbmcgui.ListItem(label=title)
        # Create a URL for a plugin recursive call.
        # Example: plugin://plugin.item.example/?action=listing&category=Animals
        url = get_url(action='listing',category=category, mid=mid,  bcaster=bcaster_name )
    
    # Set additional info for the list item.
    # setInfo allows to set various information for an item.
    # For available properties see the following link:
    # https://codedocs.xyz/xbmc/xbmc/group__python__xbmcgui__listitem.html#ga0b71166869bda87ad744942888fb5f14
    # 'mediatype' is needed for a skin to display info for this ListItem correctly.
    
    list_item.setInfo(mediatype, {'title': title,
                                #'genre': category,
                                'tagline': tagline,
                                'plot': plot,
                                'duration': duration,
                                'mediatype': mediatype})
   
    

    # Add our item to the Kodi virtual folder listing.
    xbmcplugin.addDirectoryItem(_handle, url, list_item, is_folder)
            
def list_main_categories(mid, category = catSubCat):
    """
    Create the list of item categories in the Kodi interface.
    Requests item list from the mediathek API.
    """
    # Set plugin category. It is displayed in some skins as the name
    # of the current section.
    xbmcplugin.setPluginCategory(_handle, category)
    # Set plugin content. It allows Kodi to select appropriate views
    # for this type of content.
    xbmcplugin.setContent(_handle, 'videos')

    if _mediathek == None:
        ardlogo = ARDMediathek._resource_path + 'ard.png'
        zdflogo = ZDFMediathek._resource_path + 'zdf.png'
        kikalogo = KIKAMediathek._resource_path + 'kika.png'
        insert_element('Das Erste','ard', {
                'img': ardlogo,
                'imgHi': '',
                'title': 'Das Erste',
                'subtitle': 'ARD Mediathek',
                'type': 'subcategory',
                'mid': 'home'})
        insert_element('ZDF','zdf',{
                'img': zdflogo,
                'imgHi': '',
                'title': 'ZDF',
                'subtitle': 'ZDF Mediathek',
                'type': 'subcategory',
                'mid': 'home'})
        insert_element('KIKA',KIKAMediathek.name,{
                'img': kikalogo,
                'imgHi': '',
                'title': 'KIKA',
                'subtitle': 'KIKA Mediathek',
                'type': 'subcategory',
                'mid': 'home'})
    else:
        # main categories
        if mid == 'home' and category == catSubCat and _mediathek.name != KIKAMediathek.name:  
            # show main categories in home page only  
            insert_element('Sendung von A-Z', 'atoz', None)
            insert_element(catEPG, 'time', None)
        # retrieve content from mediathek for media id  
        _mediathek.fetch_mediathek_layout(mid)

        if category == catSubCat:
            for cat in _mediathek.get_categories():
                    insert_element(cat, mid , None)
        # append elements to listing
        for e in _mediathek.get_elements(category):
            insert_element(category,mid, e) 
        # all uncategorized elements        
        for e in _mediathek.get_elements():
                insert_element(category,mid, e)  
    # Add a sort method for the virtual folder items (alphabetically, ignore articles)
    #xbmcplugin.addSortMethod(_handle, xbmcplugin.SORT_METHOD_LABEL_IGNORE_THE)
    # Finish creating a virtual folder.
    xbmcplugin.endOfDirectory(_handle)



def play_video(vid):
    """
    Play a video by the provided path.

    :param path: Fully-qualified video URL
    :type path: str
    """

    # convert id to url
    desc = _mediathek.get_video_desc(vid)
    # Create a playable item with a path to play.
    play_item = xbmcgui.ListItem(path=desc['url'])
    plot = desc['synopsis']
    
    play_item.setInfo('video',{'plot':plot})
    dbg('mime: ' + desc['type'],LOGWARNING)
    dbg('url: ' + desc['url'],LOGWARNING)
    if desc['type'] == 'application/dash+xml':
        play_item.setProperty('inputstreamaddon','inputstream.adaptive')
        play_item.setProperty('inputstream.adaptive.manifest_type','mpd')
    elif desc['type'] == 'application/mpegurl':
        play_item.setProperty('inputstreamaddon','inputstream.adaptive')
        play_item.setProperty('inputstream.adaptive.manifest_type','m3u')

    # Pass the item to the Kodi player.
    xbmcplugin.setResolvedUrl(_handle, True, listitem=play_item)


def router(paramstring):
    """
    Router function that calls other functions
    depending on the provided paramstring
    :param paramstring: URL encoded plugin paramstring
    :type paramstring: str
    """
    
    dbg('router: ' + paramstring)
    # Parse a URL-encoded paramstring to the dictionary of
    # {<parameter>: <value>} elements
    params = dict(parse_qsl(paramstring))
    global _mediathek
    #select mediathek implementation
    if 'bcaster' in params:
        if params['bcaster'] == ZDFMediathek.name:
            _mediathek = ZDFMediathek()
        elif params['bcaster'] == ARDMediathek.name:
            _mediathek = ARDMediathek()
        elif params['bcaster'] == KIKAMediathek.name:
            _mediathek = KIKAMediathek()
    # Check the parameters passed to the plugin
    if params and 'action' in params:
        if params['action'] == 'listing':
            # Display the list of videos in a provided category.
            list_main_categories(params['mid'],params['category'])
        elif params['action'] == 'play':
            # Play a video from a provided URL.
            play_video(params['vid'])
        else:
            # If the provided paramstring does not contain a supported action
            # we raise an exception. This helps to catch coding errors,
            # e.g. typos in action names.
            raise ValueError('Invalid paramstring: {0}!'.format(paramstring))
    else:
        # If the plugin is called from Kodi UI without any parameters,
        # display the list of video categories
        list_main_categories('home')


if __name__ == '__main__':
    # Call the router function and pass the plugin call parameters to it.
    # We use string slicing to trim the leading '?' from the plugin call paramstring
    try:
        router(sys.argv[2][1:])
    except MException as e:
        dialog = xbmcgui.Dialog()
        dialog.notification('ARDMediathek', e.message, xbmcgui.NOTIFICATION_ERROR, 5000)
    

